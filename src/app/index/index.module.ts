import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndexService } from './index.service';
import { IndexComponent } from './index.component';

import { FooderModule } from '../fooder/fooder.module';
import { AboutModule } from '../about/about.module';
import { DescriptionModule } from '../description/description.module';
import { DistibutionModule } from '../distibution/distibution.module';
import { TimelineModule } from '../timeline/timeline.module';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
    imports: [
        CommonModule,
        FooderModule,
        AboutModule,
        DescriptionModule,
        DistibutionModule,
        TimelineModule,
        ModalModule.forRoot()
    ],
    declarations: [
        IndexComponent
    ],
    exports: [
        IndexComponent
    ],
    providers: [
        IndexService
    ]
})
export class IndexModule { }
