import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistibutionComponent } from './distibution.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
      DistibutionComponent
  ],
  exports: [
      DistibutionComponent
]
})
export class DistibutionModule { }
