import { TestBed, inject } from '@angular/core/testing';

import { FooderService } from './fooder.service';

describe('FooderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FooderService]
    });
  });

  it('should be created', inject([FooderService], (service: FooderService) => {
    expect(service).toBeTruthy();
  }));
});
