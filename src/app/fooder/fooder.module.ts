import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooderComponent } from './fooder.component';
import { FooderService } from './fooder.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
      FooderComponent
  ],
  providers: [
      FooderService
  ],
  exports: [
      FooderComponent
  ]
})
export class FooderModule { }
